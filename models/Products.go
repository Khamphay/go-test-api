package models

import (
	"fmt"

	"gorm.io/gorm"
)

type Products struct {
	id         string `json:"ID" gorm:"primary_key"`
	name       string `json:"Name"`
	price      int32  `json:"Price"`
	quantity   int8   `json:"Quantity"`
	isDedele   bool   `json:"IsDedele" gorm:"default:false"`
	details    string `json:"Details"`
	gorm.Model        //TODO: implement some fields by default of gorm
}

func CreateProduct(db *gorm.DB, product *Products) (err error) {
	fmt.Println(product)
	err = db.Create(product).Error
	if err != nil {
		return err
	}

	return nil
}

func GetProducts(db *gorm.DB, product *[]Products) (err error) {
	err = db.Find(product).Error
	if err != nil {
		return err
	}

	return nil
}

func GetProductById(db *gorm.DB, product *Products, id string) (err error) {
	err = db.Where("id=?", id).First(product).Error
	if err != nil {
		return err
	}

	return nil
}
